#ifndef WIDGET_H
#define WIDGET_H
#include <QMainWindow>
#include <QListWidgetItem>
#include<QJsonValue>
#include<QTableWidgetItem>
#include <QFileDialog>
#include <QWidget>
#include <QDebug>
#include <QByteArray>
#include <QCoreApplication>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QListWidget>
#include <QTableWidget>
QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QMainWindow
{
	Q_OBJECT

	struct ColumnName
	{
		QString comV;
		QString ipV;
		QString nameV;
		QString osV;
		QString ncV;
		QString statusV;
		QString uuidV;
	};


	struct Mydlpclipboard {
		QString ip;
		QString uuid;
		QString version;
	};

	struct Mydlpfs {
		QString ip;
		QString uuid;
		QString version;
	};

	struct Mydlpmail {
		QString ip;
		QString uuid;
		QString version;
	};

	struct Mydlpprinter {
		QString ip;
		QString uuid;
		QString version;
	};

	struct Mydlpweb {
		QString ip;
		QString uuid;
		QString version;
	};
	struct MyTableStruct{
		ColumnName computer;
		ColumnName ip;
		Mydlpclipboard mydlpclipboard;
		Mydlpfs mydlpfs;
		Mydlpmail mydlpmail;
		Mydlpprinter mydlpprinter;
		Mydlpweb mydlpweb;
		ColumnName name;
		ColumnName nc_version;
		ColumnName os_version;
		ColumnName status;
		ColumnName uuid;

	} MyTable;

	QVector<MyTableStruct> endpoints;
	QJsonValue value;

protected:
	QVector<MyTableStruct> defineRandomData(int indexSize);



public:
	Widget(QWidget *parent = nullptr);
	~Widget();

private slots:
void on_searchButton_clicked();
void TableWidgetDisplay(QVector<MyTableStruct>MyTable);
void on_showFolderContentsButton_clicked();
void on_chooseDirectoryButton_clicked();
void on_listWidget_itemDoubleClicked(QListWidgetItem *item);
void on_clearButton_clicked();
void on_exitButton_clicked();
void on_FilterExitButton_clicked();

private:
	Ui::Widget *ui;
	QString tmpfile();
	void on_tableWidget_itemDoubleClicked(QTableWidgetItem *item);
	QString Filter;
	QListWidgetItem item;
};


#endif // WIDGET_H
